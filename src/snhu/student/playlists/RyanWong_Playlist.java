package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class RyanWong_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	//this creates the playlist
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	//create list for one song from original project
	ArrayList<Song> beatlesTracks = new ArrayList<Song>();
    TheBeatles theBeatlesBand = new TheBeatles();
	
    beatlesTracks = theBeatlesBand.getBeatlesSongs();
	//add song to the playlist
	playlist.add(beatlesTracks.get(0));
	playlist.add(beatlesTracks.get(1));
	
	//get songs from the created BetweenTheBuriedAndMe band in module 5 
    BetweenTheBuriedAndMe BetweenTheBuriedAndMeBand = new BetweenTheBuriedAndMe();
	ArrayList<Song> BetweenTheBuriedAndMeTracks = new ArrayList<Song>();
    BetweenTheBuriedAndMeTracks = BetweenTheBuriedAndMeBand.getBetweenTheBuriedAndMeSongs();
	
	playlist.add(BetweenTheBuriedAndMeTracks.get(0));
	playlist.add(BetweenTheBuriedAndMeTracks.get(1));
	playlist.add(BetweenTheBuriedAndMeTracks.get(2));
	
	//get songs form the created WuTangClan band in module 5 
	WuTangClan WuTangClanBand = new WuTangClan();
	ArrayList<Song> WuTangClanTracks = new ArrayList<Song>();
    WuTangClanTracks = WuTangClanBand.getWuTangClanSongs();
	
	playlist.add(WuTangClanTracks.get(0));
	playlist.add(WuTangClanTracks.get(1));
	
	//get one song from chosen classmate, CocteauTwins
	CocteauTwins CocteauTwinsBand = new CocteauTwins();
	ArrayList<Song> CocteauTwinsTracks = new ArrayList<Song>();
    CocteauTwinsTracks = CocteauTwinsBand.getCocteauTwinsSongs();
	
	playlist.add(CocteauTwinsTracks.get(0));
	
    return playlist;
	}

}
