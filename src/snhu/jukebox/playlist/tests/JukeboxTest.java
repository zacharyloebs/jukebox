package snhu.jukebox.playlist.tests;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Test;
import music.artist.*;
import snhu.jukebox.playlist.Song;

public class JukeboxTest {

	@Test
	public void testDanceGavinDanceAlbumSize() throws NoSuchFieldException, SecurityException {
		 DanceGavinDance DanceGavinDanceBand = new DanceGavinDance();
		 ArrayList<Song> DanceGavinDanceTracks = new ArrayList<Song>();
		 DanceGavinDanceTracks = DanceGavinDanceBand.getDanceGavinDanceSongs();
		 assertEquals(3, DanceGavinDanceTracks.size());
	}
	
	@Test
	public void testCircaSurviveAlbumSize() throws NoSuchFieldException, SecurityException {
		 CircaSurvive CircaSurviveBand = new CircaSurvive();
		 ArrayList<Song> CircaSurviveTracks = new ArrayList<Song>();
		 CircaSurviveTracks = CircaSurviveBand.getCircaSurviveSongs();
		 assertEquals(3, CircaSurviveTracks.size());
	}
	
	@Test
	public void testGetBeatlesAlbumSize() throws NoSuchFieldException, SecurityException {
		 TheBeatles theBeatlesBand = new TheBeatles();
		 ArrayList<Song> beatlesTracks = new ArrayList<Song>();
		 beatlesTracks = theBeatlesBand.getBeatlesSongs();
		 assertEquals(2, beatlesTracks.size());
	}
	
	@Test
	public void testGetImagineDragonsAlbumSize() throws NoSuchFieldException, SecurityException {
		 ImagineDragons imagineDragons = new ImagineDragons();
		 ArrayList<Song> imagineDragonsTracks = new ArrayList<Song>();
		 imagineDragonsTracks = imagineDragons.getImagineDragonsSongs();
		 assertEquals(3, imagineDragonsTracks.size());
	}
	
	@Test
	public void testGetAdelesAlbumSize() throws NoSuchFieldException, SecurityException {
		 Adele adele = new Adele();
		 ArrayList<Song> adelesTracks = new ArrayList<Song>();
		 adelesTracks = adele.getAdelesSongs();
		 assertEquals(3, adelesTracks.size());
	}
	//added my tracks as test
	@Test
	public void testGetBetweenTheBuriedAndMeAlbumSize() throws NoSuchFieldException, SecurityException {
		 BetweenTheBuriedAndMe BetweenTheBuriedAndMe = new BetweenTheBuriedAndMe();
		 ArrayList<Song> BetweenTheBuriedAndMeTracks = new ArrayList<Song>();
		 BetweenTheBuriedAndMeTracks = BetweenTheBuriedAndMe.getBetweenTheBuriedAndMeSongs();
		 assertEquals(3, BetweenTheBuriedAndMeTracks.size());
	}
	//added my tracks as test
	@Test
	public void testGetWuTangClanAlbumSize() throws NoSuchFieldException, SecurityException {
		 WuTangClan WuTangClan = new WuTangClan();
		 ArrayList<Song> WuTangClanTracks = new ArrayList<Song>();
		 WuTangClanTracks = WuTangClan.getWuTangClanSongs();
		 assertEquals(2, WuTangClanTracks.size());
	}
	//added choose track from classmate as test
	@Test
	public void testGetCocteauTwinsAlbumSize() throws NoSuchFieldException, SecurityException {
		 CocteauTwins CocteauTwins = new CocteauTwins();
		 ArrayList<Song> CocteauTwinsTracks = new ArrayList<Song>();
		 CocteauTwinsTracks = CocteauTwins.getCocteauTwinsSongs();
		 assertEquals(1, CocteauTwinsTracks.size());
	}
}
