package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class DanceGavinDance {

	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public DanceGavinDance() {
    }
    
    public ArrayList<Song> getDanceGavinDanceSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("The Robot with Human Hair Pt. 2 1/2", "DanceGavinDance");             //Create a song
         Song track2 = new Song("Tree Village", "DanceGavinDance");     //Create another song
         Song track3 = new Song("Chucky vs. The Giant Tortoise", "DanceGavinDance");
         
         this.albumTracks.add(track1);                                          
         this.albumTracks.add(track2);   
         this.albumTracks.add(track3);
         
         return albumTracks;      
    }
}
