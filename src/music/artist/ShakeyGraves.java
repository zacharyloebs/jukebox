package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

/* Added new band file Shakey Graves
 * Shakey Graves is an American Americana musician. 
 * His music combines blues, folk, and rock and roll. 
 */

public class ShakeyGraves {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public ShakeyGraves() {
    }
    
    // Song from 2013 live album
    public ArrayList<Song> getShakeyGravesSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Roll the Bones", "Shakey Graves");             //Create a song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Shakey Graves                                        
         return albumTracks;                                                    //Return the songs for Shakey Graves in the form of an ArrayList
    }
}
