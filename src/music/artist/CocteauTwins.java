package music.artist;

import java.util.ArrayList;
import snhu.jukebox.playlist.Song;

public class CocteauTwins {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public ArrayList<Song> getCocteauTwinsSongs() {
    	        	
       	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
       	 Song track1 = new Song("Cherry-coloured Funk", "Cocteau Twins");       //Create a song
            this.albumTracks.add(track1);                                      //Add the first song to song list for Cocteau Twins
            return albumTracks;                                                //Return the songs for Cocteau Twins in the form of an ArrayList
       }

}
