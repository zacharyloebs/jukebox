package music.artist;

import java.util.ArrayList;
import snhu.jukebox.playlist.Song;

public class BetweenTheBuriedAndMe {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public BetweenTheBuriedAndMe() {
    }
    
    public ArrayList<Song> getBetweenTheBuriedAndMeSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                            //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Informal Gluttony", "Between The Buried and Me");       //Create a song
         Song track2 = new Song("White Walls", "Between The Buried and Me");             //Create another song
         Song track3 = new Song("Demons", "Between The Buried and Me");                  //Create a third song
         this.albumTracks.add(track1);                                                   //Add the first song to song list for Between The Buried and Me
         this.albumTracks.add(track2);                                                   //Add the second song to song list for Between The Buried and Me 
         this.albumTracks.add(track3);                                                   //Add the third song to song list for Between The Buried and Me
         return albumTracks;                                                             //Return the songs for Between The Buried and Me in the form of an ArrayList
    }

}
