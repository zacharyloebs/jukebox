package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class CircaSurvive {

	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public CircaSurvive() {
    }
    
    public ArrayList<Song> getCircaSurviveSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Get Out", "Circa Survive");             //Create a song
         Song track2 = new Song("Child of the Desert", "CircaSurvive");     //Create another song
         Song track3 = new Song("Wish Resign", "Circa Survive");
         
         this.albumTracks.add(track1);                                          
         this.albumTracks.add(track2);   
         this.albumTracks.add(track3);
         
         return albumTracks;      
    }
}