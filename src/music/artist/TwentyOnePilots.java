package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

/* Created new band file TwentyOnePilots
 * Twenty One Pilots is an American musical duo from Columbus, Ohio.
 */

public class TwentyOnePilots {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public TwentyOnePilots() {
    }
    
    // Songs from 2015 album Blurryface
    
    public ArrayList<Song> getTwentyOnePilotsSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   		//Instantiate the album so we can populate it below
    	 Song track1 = new Song("Ride", "Twenty One Pilots");             				//Create a song
         Song track2 = new Song("The Judge", "Twenty One Pilots");         				//Create another song
    	 Song track3 = new Song("Stressed Out", "Twenty One Pilots");             		//Create a third song
         Song track4 = new Song("Heaveydirtysoul", "Twenty One Pilots");   				// Created a fourth song
         this.albumTracks.add(track1);                                          		//Add the first song to song list for Twenty One Pilots
         this.albumTracks.add(track2);                                          		//Add the second song to song list for Twenty One Pilots 
         this.albumTracks.add(track3);                                          		//Add the third song to song list for Twenty One Pilots
         this.albumTracks.add(track4);                                          		//Add the fourth song to song list for Twenty One Pilots 

         return albumTracks;                                                    		//Return the songs for Twenty One Pilots in the form of an ArrayList
    }
}
