package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class WuTangClan {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public WuTangClan() {
    }
    
    public ArrayList<Song> getWuTangClanSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                            //Instantiate the album so we can populate it below
    	 Song track1 = new Song("Triump", "Wu-Tang Clan");               //Create a song
         Song track2 = new Song("Protect Ya Neck", "Wu-Tang Clan");      //Create another song
         this.albumTracks.add(track1);                                   //Add the first song to song list for Wu-Tang Clan
         this.albumTracks.add(track2);                                   //Add the second song to song list for Wu-Tang Clan
         return albumTracks;                                             //Return the songs for Wu-Tang Clan in the form of an ArrayList
    }

}
